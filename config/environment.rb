APP_ROOT = "#{File.dirname(__FILE__)}/.." unless defined?(APP_ROOT)

require 'yaml'

APP_CONFIG = YAML.load_file("#{APP_ROOT}/config/settings.yml")

Dir["#{APP_ROOT}/lib/*.rb"].each {|file| require file }

