require 'nokogiri'
require 'open-uri'
require 'json'

# Address
# city
# state
# Rent
# Number of bedrooms
# Number of bathrooms
# Full description
# Photos

class Import
  def initialize
  end

  def self.export!
    feeds = APP_CONFIG['feeds']

    items = []

    feeds.each do |feed_url|
      doc = Nokogiri::XML(open(feed_url))
      doc.css('item').each do |item|

        item_url = item.xpath('link').inner_text
        title = item.xpath('title').inner_text

        description = item.xpath('description').inner_text
        description_doc = Nokogiri::XML(description)

        bedrooms, bathrooms = description_doc.css('b')[0].parent.inner_text.scan(/\d+/)
        price = description_doc.css('b')[2].parent.inner_text.delete('^0-9.')

        # Go to single item page
        #item_url = 'http://abbarealestate.vflyer.com/home/flyer/home/264521015'
        detailed_doc = Nokogiri::HTML(open(item_url))

        item_uri = URI.parse(item_url)
        detailed_attributes = detailed_doc.xpath('//embed').first.nil? ? self.parse_type2(detailed_doc, item_uri) : self.parse_type1(detailed_doc)

        new_item = {
            :title => title,
            :rent => price,
            :bedrooms => bedrooms,
            :full_bathrooms => bathrooms
        }.merge!(detailed_attributes)

        items << new_item

        puts "Get info about '#{title}'"

        self.import_request([new_item])
      end
    end

    items
  end

  def self.import!
    items = self.export!
    #self.import_request(items)
  end

  def self.import_request(items)
    listings = {
        :listings => items
    }

    uri = URI.parse(APP_CONFIG['api']['url'])
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http.read_timeout = 500
    http.open_timeout = 500

    request = Net::HTTP::Post.new(APP_CONFIG['api']['path'])

    request.add_field('Accept', 'application/json')
    request.add_field('Content-Type', 'application/json')

    request.basic_auth APP_CONFIG['api']['login'], APP_CONFIG['api']['password']

    request.body = listings.to_json
    response = http.request(request)
    puts response.body.inspect
  end

  def self.parse_type1(detailed_doc)
    address = detailed_doc.css('div.ItemGroup')[2].css('div.subItemTextNarrow')[0].nil? ? '' : detailed_doc.css('div.ItemGroup')[2].css('div.subItemTextNarrow')[0].inner_text
    if address.include?('Bedrooms') || address.empty?
      address, city, state, zip = '.', '', '', ''
    else
      city_state_zip = detailed_doc.css('div.ItemGroup')[2].css('div.subItemTextNarrow')[2].nil? ? '' : detailed_doc.css('div.ItemGroup')[2].css('div.subItemTextNarrow')[2].inner_text
      city, state, zip = self.parse_city_state_zip(city_state_zip)
    end

    description = detailed_doc.css('div.subItemTextWide')[0].nil? ? '' : detailed_doc.css('div.subItemTextWide')[0].inner_html
    description.gsub!('<br clear="none">', "\n")
    description = Nokogiri::HTML(description).text

    # Go to images list
    images = []
    unless detailed_doc.xpath('//embed').first.nil?
      images_url = detailed_doc.xpath('//embed').first['flashvars'].gsub('url=', '')
      images_doc = Nokogiri::HTML(open(images_url))
      images_doc.xpath('//large').each do |image_url|
        images << image_url.inner_text
      end
    end

    {
        :images => images,
        :address => address,
        :city => city,
        :state => state,
        :zip => zip,
        :highlights => description
    }
  end

  def self.parse_type2(detailed_doc, item_uri)
    address = detailed_doc.css('div.subItemTextNarrow')[2].css('span')[0].nil? ? '.' : detailed_doc.css('div.subItemTextNarrow')[2].css('span')[0].inner_text
    city_state_zip = detailed_doc.css('div.subItemTextNarrow')[2].css('span')[2].nil? ? '' : detailed_doc.css('div.subItemTextNarrow')[2].css('span')[2].inner_text
    city, state, zip = self.parse_city_state_zip(city_state_zip)

    description = detailed_doc.css('div.itemContainer')[0].nil? ? '' : detailed_doc.css('div.itemContainer')[0].inner_html
    description.gsub!('<br clear="none">', "\n")
    description = Nokogiri::HTML(description).text

    images = []
    unless detailed_doc.css('a[@href*=photogallery]')[0].nil?
      images_url = item_uri.scheme + '://' + item_uri.host + detailed_doc.css('a[@href*=photogallery]')[0]['href']
      images_doc = Nokogiri::HTML(open(images_url))

      images_doc.css('div.tool-tip img').each do |image_url|
        images << image_url['src']
      end
    end

    {
        :images => images,
        :address => address,
        :city => city,
        :state => state,
        :zip => zip,
        :highlights => description
    }
  end

  def self.parse_city_state_zip(str)
    city, state_zip = str.split(',')
    state = state_zip.delete('0-9 ')
    zip = state_zip.delete('^0-9')

    [city, state, zip]
  end

end